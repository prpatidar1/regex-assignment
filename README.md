Using regular expressions, you have to implement the functionality below.


1. To validate Password which includes 

    - Uppercase (A-Z) and lowercase (a-z) English letters.

    - Digits (0-9).

    - Characters ! # $ % & ' * + - / = ? ^ _ ` { | } ~

    - Character. ( period, dot or full stop) provided that it is not the first or last character and it  

      will not come one after the other.


2. To validate an Email address


3. To validate Credit Card pattern


4. Write a method to distinguish and pick the values of email address, phone number from the below paragraph:

    Lorem ipsum dolor 9221122108 sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor sed viverra ipsum nunc aliquet bibendum enim. In massa tempor nec feugiat. Nunc aliquet bibendum enim facilisis gravida. mytraining@deqode.com Nisl nunc mi ipsum faucibus vitae aliquet nec ullamcorper. Amet luctus venenatis lectus magna fringilla. Volutpat maecenas volutpat blandit aliquam etiam erat velit scelerisque in. Egestas egestas fringilla phasellus faucibus scelerisque eleifend. +91-20200-21210 Sagittis orci a scelerisque purus semper eget duis. Nulla pharetra diam sit amet nisl suscipit. Sed adipiscing diam donec adipiscing tristique risus nec feugiat in. Fusce (+91)-20200-21210 ut placerat mt@test.inc orci nulla. Pharetra vel turpis nunc eget lorem dolor. Tristique senectus et netus et malesuada.

